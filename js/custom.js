jQuery(document).ready(function () {

    //Wow
    wow = new WOW({

        mobile: false // default
    })
    wow.init();

    // Slider section
    jQuery('.software_package_slider').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 5,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
    });
    
    //Input Field
    var formFields = jQuery('.form-group');

    formFields.each(function() {
        var field = jQuery(this);
        var input = field.find('.form-control');
        var label = field.find('label');

        function checkInput() {
            var valueLength = input.val().length;
            if (valueLength > 0 ) {
                label.addClass('freeze')
            } 
            else {
                label.removeClass('freeze')
            }
        }
        input.change(function() {
            checkInput()
        })
    });
    
    // Profile Upload

    jQuery(".profile-img-sec input:file").on('change', function (){
        var fileName = jQuery(this).val();
    });

    //file input preview
    function readURL(input) { 
        if (input.files && input.files[0]) {
        var reader = new FileReader();
            reader.onload = function (e) {
                jQuery('.profile-img-sec figure').attr('style', 'background-image: url('+e.target.result   +')');  
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    jQuery('body').find(".profile-img-sec input[type='file']").on('change',function(){
        readURL(this);
    });
    
    
    
    
    // Upload Image
    
    function readsURL(input) { 
        console.log(input.files["0"].name);
        if (input.files && input.files[0]) {
        var uimage = new FileReader();
            uimage.readAsDataURL(input.files[0]);
             jQuery(".upload-image #uploadFile").val(input.files["0"].name);
        }
    }
    jQuery('body').find(".upload-image input[type='file']").on('change',function(){
        readsURL(this);
    });
    
    
    
    // Upload Document
    
    function read(input) { 
        console.log(input.files["0"].name);
        if (input.files && input.files[0]) {
        var document = new FileReader();
            document.readAsDataURL(input.files[0]);
             jQuery(".upload-document #uploadFile1").val(input.files["0"].name);
        }
    }
    jQuery('body').find(".upload-document input[type='file']").on('change',function(){
        read(this);
    });
    
    
    
    // Edit document
    
    jQuery(".image-doc .upload-document input:file").on('change', function (){
        var fileName = jQuery(this).val();
    });
    
    //file input preview
    function readIMG(input) { 
        if (input.files && input.files[0]) {
        var readerimg = new FileReader();
            readerimg.onload = function (e) {
                jQuery('.image-doc .upload-image-sec').attr('style', 'background-image: url('+e.target.result   +')');  
            }
            readerimg.readAsDataURL(input.files[0]);
        }
    }
    jQuery('body').find(".image-doc .upload-document input[type='file']").on('change',function(){
        readIMG(this);
    });
});


// Custom Scrollbar

jQuery(window).on("load", function () {
    jQuery(".inbox-wrap ul, .inbox-wrap .chat-box-sec").mCustomScrollbar();
});

//Header

jQuery(window).on("load resize scroll", function(e) {
  var Win = jQuery(window).height();
  var Header = jQuery("header").height();
  var Footer = jQuery("footer").height();

  var NHF = Header + Footer;

  jQuery('.main').css('min-height', (Win - NHF));

});

//Page Zoom

document.documentElement.addEventListener('touchstart', function (event) {
 if (event.touches.length > 1) {
   event.preventDefault();
 }
}, false);

//Model
jQuery(function() {
  //----- OPEN
  jQuery('[data-popup-open]').on('click', function(e) {
    var targeted_popup_class = jQuery(this).attr('data-popup-open');
    jQuery('[data-popup="' + targeted_popup_class + '"]').fadeIn(100);

    e.preventDefault();
  });

  //----- CLOSE
  jQuery(document).on('click','[data-popup-close]','.cstmClose',function(e) {
    var targeted_popup_class = jQuery(this).attr('data-popup-close');
    jQuery('[data-popup="' + targeted_popup_class + '"]').fadeOut(200);

    e.preventDefault();
  });
});

function onScroll(event) {
    var scrollPos = jQuery(document).scrollTop();
    jQuery('.navbar-nav li a').each(function () {
        var currLink = jQuery(this);
        var refElement = jQuery(currLink.attr("href"));
        console.log(refElement);
        if (jQuery(refElement).length > 0 ) { 
            if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                jQuery('.navbar-nav li a').removeClass("active");
                currLink.addClass("active");
            }
            else {
                currLink.removeClass("active");
            }
        }
    });
}

//Textarea 

jQuery(function (jQuery) {
    jQuery('.firstCap, textarea').on('keypress', function (event) {
        var jQuerythis = jQuery(this),
            thisVal = jQuerythis.val(),
            FLC = thisVal.slice(0, 1).toUpperCase();
        con = thisVal.slice(1, thisVal.length);
        jQuery(this).val(FLC + con);
    });
});